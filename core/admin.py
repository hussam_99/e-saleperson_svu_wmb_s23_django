from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .models import Region, User, Sales

# Register your models here.

@admin.register(User)
class UserAdmin(BaseUserAdmin):
    search_fields = ['username']
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "password1", "password2","email","first_name","last_name","job_number","image"),
            },
        ),
    )
    list_display = ['id','username',"email","first_name","last_name","job_number","image"]


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(Sales)
class SalesAdmin(admin.ModelAdmin):
    list_display = ('salesperson', 'region', 'date', 'sales_amount', 'score')
    list_filter = ('region', 'date')
    search_fields = ('salesperson__username', 'region__name')
