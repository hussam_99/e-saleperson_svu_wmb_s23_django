
from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class Region(models.Model):
    id = models.IntegerField(primary_key=True)
    name =  models.CharField(max_length=50)
    def __str__(self) :
        return self.name

class User(AbstractUser):
    job_number = models.CharField(max_length=10, null = True , blank = True)
    image = models.FileField(upload_to='media/core/images', null = True , blank = True)
    region = models.ForeignKey('Region', on_delete=models.CASCADE, null = True , blank = True)

    def __str__(self):
        return self.username
        
class Sales(models.Model):
    salesperson = models.ForeignKey(User, on_delete=models.CASCADE)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    date = models.DateField()
    sales_amount = models.DecimalField(max_digits=10, decimal_places=2)
    score = models.DecimalField(max_digits=10, decimal_places=2)
