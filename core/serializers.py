from djoser.serializers import UserCreateSerializer
from rest_framework import serializers
from djoser.serializers import TokenSerializer
from .models import User, Region, Sales



class CustomUserCreateSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = User
        fields = ('id', 'username','password', 'job_number', 'image', 'region')



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'job_number', 'image', 'region']

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields = '__all__'

class SalesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sales
        fields = '__all__'


class MonthlyScoreResultSerializer(serializers.Serializer):
    score = serializers.DecimalField(max_digits=100000, decimal_places=2)
    salesperson = serializers.IntegerField()
    region = serializers.IntegerField()
        