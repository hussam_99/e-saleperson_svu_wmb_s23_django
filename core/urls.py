from django.urls import path
from core.views import SalesByMonth_YearAPIView,SalesByMonthAPIView, SalesByYearAPIView, UserDeleteView, MonthlyScoreView,CustomTokenObtainPairView, RegionView, UserUpdateView , UserDetail, SalesList, SalesDetail

urlpatterns = [
    # Other URL patterns
    path('auth/jwt/token/', CustomTokenObtainPairView.as_view(), name='custom_token_obtain_pair'),
    path('region/', RegionView.as_view(), name='region'),
    path('modify/<int:pk>/', UserUpdateView.as_view(), name='user-update'),
    path('user/detail/<int:pk>/', UserDetail.as_view(), name='user-detail'),
    path('api/sales/', SalesList.as_view(), name='sales-list'),
    path('api/sales/<int:pk>/', SalesDetail.as_view(), name='sales-detail'),
    path('calculate-monthly-score/<int:month>/<int:region>/<int:salesperson>/', MonthlyScoreView.as_view(), name='calculate-monthly-score'),
    path('auth/delete/<int:pk>/', UserDeleteView.as_view(), name='user-delete'),
    path('sales-by-month/<int:salesperson>/', SalesByMonthAPIView.as_view(), name='sales-by-month'),
    path('sales-by-year/<int:salesperson>/', SalesByYearAPIView.as_view(), name='sales-by-year'),
    path('sales-by-year_month/<int:salesperson>/', SalesByMonth_YearAPIView.as_view(), name='sales-by-year'),


   


    # Add more URL patterns if needed
]
