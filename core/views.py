from django.shortcuts import render
from rest_framework import generics
from .models import User, Region, Sales
from .serializers import MonthlyScoreResultSerializer,  UserSerializer, RegionSerializer, CustomUserCreateSerializer, SalesSerializer, SalesSerializer
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from django.db.models import Sum
from django.db.models.functions import ExtractMonth, ExtractYear
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from rest_framework import serializers

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        data['user_name'] = self.user.username
        data['is_staff'] = self.user.is_staff
        data['image'] = self.get_absolute_image_url(self.user.image)
        data['region'] = self.serialize_region(self.user.region)  # Serialize the region
        data['id'] = self.user.id
        return data

    def serialize_region(self, region):
        if region:
            return {
                'id': region.id,
                'name': region.name,
                # Add any other relevant attributes here
            }
        return None

    def get_absolute_image_url(self, image):
        if image:
            request = self.context.get("request")
            if request is not None:
                return request.build_absolute_uri(image.url)
        return None


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer


class RegionView(generics.ListCreateAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer



class UserUpdateView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDeleteView(generics.DestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class SalesList(generics.ListCreateAPIView):
    queryset = Sales.objects.all()
    serializer_class = SalesSerializer

class SalesDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sales.objects.all()
    serializer_class = SalesSerializer



class MonthlyScoreView(generics.ListAPIView):
    serializer_class = MonthlyScoreResultSerializer

    def get_queryset(self):
        # Get the month, region, and salesperson from the URL kwargs
        month = self.kwargs.get('month')
        region = self.kwargs.get('region')
        salesperson = self.kwargs.get('salesperson')

        # Filter the Sales objects for the given month, region, and salesperson
        queryset = Sales.objects.filter(date__month=month, region=region, salesperson=salesperson)

        # Calculate the sum of scores for the filtered queryset
        total_score = queryset.aggregate(total_score=Sum('score'))['total_score']

        # Create a dictionary with the total score, salesperson, and region
        result = {
            "score": total_score if total_score is not None else 0,  # Return 0 if total_score is None
            "salesperson": salesperson,
            "region": region
        }

        # Return a list containing the result
        return [result]
class SalesByMonth_YearAPIView(generics.ListAPIView):
    serializer_class = SalesSerializer

    def get_queryset(self):
        month = self.request.query_params.get('month')
        year = self.request.query_params.get('year')
        return Sales.objects.filter(salesperson=self.kwargs['salesperson'], date__month=month, date__year=year)

class SalesByMonthAPIView(generics.ListAPIView):
    serializer_class = SalesSerializer

    def get_queryset(self):
        month = self.request.query_params.get('month')
        return Sales.objects.filter(salesperson=self.kwargs['salesperson'], date__month=month)


class SalesByYearAPIView(generics.ListAPIView):
    serializer_class = SalesSerializer

    def get_queryset(self):
        year = self.request.query_params.get('year')
        return Sales.objects.filter(salesperson=self.kwargs['salesperson'], date__year=year)